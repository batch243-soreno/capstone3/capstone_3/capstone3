require("dotenv").config();

const mongoose = require("mongoose");

const connectionStr = process.env.MONGODB_CONNECTION;

mongoose
  .connect(connectionStr, { useNewUrlparser: true })
  .then(() => console.log("[✓]Connected to MongoDB database. 👍👍"))
  .catch((err) => console.log(err));

mongoose.connection.on("error", (err) => {
  console.log(err);
});
