import axios from "../axios";
import React, { useEffect } from "react";
import { Button, Carousel, Col, Container, Nav, Row } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { Link } from "react-router-dom";
import categories from "../categories";
import "./Home.css";
import { useDispatch, useSelector } from "react-redux";
import { updateProducts } from "../features/productSlice";
import ProductPreview from "../components/ProductPreview";
import photo from "../images/banner.jpg";
import feature1 from "../images/feature1.jpg";
import feature2 from "../images/feature2.jpg";
import feature3 from "../images/feature3.jpg";
// import banner from "../images/heroBanner.jpg";

function Home() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products);
  const lastProducts = products.slice(0, 8);
  useEffect(() => {
    axios.get("/products").then(({ data }) => dispatch(updateProducts(data)));
  }, []);

  return (
    <div>
      <div className="herroBanner">
        {/* Need to change this section, change into component */}
        <Row>
          <Col className="phoneDesc">
            <h3>Xperia 1 III</h3>
            <h6>
              With Xperia’s signature speed at its core, the Xperia 1 IV has a
              triple lens camera that can record 4K HDR 120fps on all lenses.12
              It can capture photos at 20fps burst with AE/AF in HDR3 and boasts
              a new 85-125mm true optical zoom. And it lets you share your
              experiences on the go with video4 and game live streaming.5
            </h6>
            <Button variant="primary" id="learnMoreButton">
              <Nav.Link href="http://localhost:3000/product/63beb73b93a20e1a816e5d12">
                Learn More
              </Nav.Link>
            </Button>{" "}
          </Col>
          <Col></Col>
        </Row>
      </div>
      <div className="featured-products-container container mt-4">
        <Container className="featureProducts">
          <Row>
            <Col sm={8}>
              <h2>Featured Products</h2>
              <Carousel>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    img
                    src={feature1}
                    alt="First slide"
                  />
                  <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={feature2}
                    alt="Second slide"
                  />

                  <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={feature3}
                    alt="Third slide"
                  />

                  <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>
                      Praesent commodo cursus magna, vel scelerisque nisl
                      consectetur.
                    </p>
                  </Carousel.Caption>
                </Carousel.Item>
              </Carousel>
            </Col>
            <Col>
              <div></div>
            </Col>
          </Row>
        </Container>
        <div className="latestProductSection">
          <h2>Our Latest Products</h2>

          {/* PRODUCT LISTS */}
          <div className="d-flex justify-content-left flex-wrap">
            {lastProducts.map((product) => (
              <ProductPreview {...product} />
            ))}
          </div>
        </div>

        <div>
          <Link
            to="/category/all"
            style={{
              textAlign: "center",
              display: "block",
              textDecoration: "none",
            }}
          >
            <button type="button" class="btn btn-secondary btn-sm">
              See More{">>"}
            </button>
          </Link>
        </div>
      </div>
      {/* sale banner */}
      <div className="sale__banner--container mt-4">
        <img src={photo}></img>
      </div>

      {/* CATEGORIES */}
      <div className="recent-products-container container mt-4">
        <h2>Categories</h2>
        <Row className="categoryContainer">
          {categories.map((category) => (
            <LinkContainer
              to={`/category/${category.name.toLocaleLowerCase()}`}
            >
              <Col md={4}>
                <div
                  style={{
                    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${category.img})`,
                    gap: "10px",
                  }}
                  className="category-tile"
                >
                  {category.name}
                </div>
              </Col>
            </LinkContainer>
          ))}
        </Row>
      </div>
    </div>
  );
}

export default Home;
